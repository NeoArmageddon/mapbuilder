#include "defines.hpp"

class MB_Main
{
	idd = 123;
	movingenable = false;
	onLoad = "uiNamespace setVariable ['mb_main_dialog', (_this select 0)];";
	onUnload = "call MB_fnc_Exit;";
	
	class controlsBackground {
		class Rsc_MouseArea : RscText {
			idc = 170001;
			style = ST_MULTI;
			
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			h = "safezoneH";
			
			text = "";
			onMouseMoving = "MB_MousePosition=[_this select 1,_this select 2];";
			onMouseEnter = "";
			onMouseExit = "";
			onMouseButtonDown = "_this call MB_fnc_MouseDown;";
			onMouseButtonUp = "_this call MB_fnc_MouseUp;";
			onMouseButtonDblClick = "_this call MB_fnc_MouseDblClick;";
			onMouseZChanged = "_this call MB_fnc_MouseZ;";
			onKeyDown = "_this call MB_fnc_KeyDown;";
			onKeyUp = "_this call MB_fnc_KeyUp;";
		};
	};
	
	class controls {
		class Rsc_Background : RscText { //--- Render out.
			idc = 170002;
			text = "";
			x = "SafeZoneX + (SafeZoneW * 0.8)";
			y = "SafeZoneY + (SafezoneH * 0)";
			w = "SafeZoneW * 0.20";
			h = "SafeZoneH * 1";
			colorBackground[] = {0, 0, 0, 0.0};
		};
	};
};
